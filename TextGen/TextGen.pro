#-------------------------------------------------
#
# Project created by QtCreator 2016-05-25T14:51:57
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = TextGen
TEMPLATE = app
CONFIG += c++14

SOURCES += main.cpp\
        mainwindow.cpp \
    markovgentext.cpp

# Не собирается? А и не должно.
# Чтобы собиралось надо указать переменные окружения
# BOOST_INCLUDE_DIR и BOOST_LIB_DIR. Это можно сделать либо глобально
# (способ задания зависит от ОС), либо через *.pro.user файл.
# Переходим к закладке Проекты, выбираем необходимый бандл для сборки
# в нем выбираем Сборка. После этого раскрываем (Подробнеее) список переменных
# окружения текущего проекта, который называется Среда сборки. Там добавляем
# требуемые значения.
#
# А чтобы увидеть текущие значения перменных можно раскомментировать
# следующие три тсроки.
#
#message("The project require follow paths:")
#message("BOOST includes:" $$(BOOST_INCLUDE_DIR))
#message("BOOST libraries:" $$(BOOST_LIB_DIR) )
!exists($$(BOOST_INCLUDE_DIR)): error("Required boost includes")
!exists($$(BOOST_LIB_DIR)): error("Required boost libraries")

HEADERS  += mainwindow.h \
    markovgentext.h

LIBS += -L"$$(BOOST_LIB_DIR)"

windows {
    LIBS += -lboost_log-mgw49-mt-1_60 \
            -lboost_log_setup-mgw49-mt-1_60 \
            -lboost_filesystem-mgw49-mt-1_60 \
            -lboost_regex-mgw49-mt-1_60 \
            -lboost_thread-mgw49-mt-1_60 \
            -lboost_system-mgw49-mt-1_60 \
            -lboost_chrono-mgw49-mt-1_60 \
            -lboost_program_options-mgw49-mt-1_60 \
            -lwsock32 -lws2_32 \
            -lpthread
}

INCLUDEPATH += "$$(BOOST_INCLUDE_DIR)"

FORMS    += mainwindow.ui
