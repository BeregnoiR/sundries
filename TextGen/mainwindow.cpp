#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QTreeWidgetItem>
#include <boost\chrono.hpp>


using namespace boost::chrono;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent), ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    connect(ui->GenText_menu, SIGNAL(triggered()), this, SLOT(onGenTextDown()));
    connect(ui->GenBase_menu, SIGNAL(triggered()), this, SLOT(onGenBaseDown()));
    mMarkovGenText = new MarkovGenText(2);
    ui->GenText_menu->setEnabled(false);
}

MainWindow::~MainWindow()
{
    delete ui;
    delete mMarkovGenText;
}

void MainWindow::onGenTextDown()
{

    QString str = QString::fromStdString(mMarkovGenText->GenText(30));
    ui->plainTextEdit_2->setPlainText(str);
//    ui->plainTextEdit->paste();
//    QString str = ui->plainTextEdit->toPlainText();
//    QTreeWidgetItem *root = new QTreeWidgetItem(ui->treeWidget);
//    root->setText(0, "112");
//    QTreeWidgetItem *vet = new QTreeWidgetItem(root);
//    vet->setText(0, "222");
//    root->addChild(vet);
    //    ui->treeWidget->addTopLevelItem(root);
}

void MainWindow::onGenBaseDown()
{
    ui->GenText_menu->setEnabled(true);
    std::string str = ui->plainTextEdit->toPlainText().toStdString();
    high_resolution_clock::time_point TimeBegin(high_resolution_clock::now());
    mMarkovGenText->Add(str);
    high_resolution_clock::time_point TimeEnd(high_resolution_clock::now());
    duration<double> durationAdd = TimeEnd - TimeBegin;
    __int64 durationAdd_mic = duration_cast<microseconds>(durationAdd).count();
    QString qstr;
    qstr.sprintf("%d", durationAdd_mic);
    ui->plainTextEdit_2->setPlainText(qstr);
    const std::map<Prefix, std::vector<std::string> > &stateTable =
            mMarkovGenText->ShowStateTable();
    for (auto i: stateTable)
    {
        QTreeWidgetItem *root = new QTreeWidgetItem(ui->treeWidget);
        std::string str;
        for (auto j : i.first)
            str += j;
        root->setText(0, str.c_str());
        for (auto j : i.second)
        {
          QTreeWidgetItem *vet = new QTreeWidgetItem(root);
          vet->setText(0, j.c_str());
          root->addChild(vet);
        }
        ui->treeWidget->addTopLevelItem(root);
    }
}
