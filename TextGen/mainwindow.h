#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "markovgentext.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    MarkovGenText *mMarkovGenText;
private slots:
    void onGenTextDown();
    void onGenBaseDown();
};

#endif // MAINWINDOW_H
