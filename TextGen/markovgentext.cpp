#include "markovgentext.h"
//------------------------------------------------------------------------------
MarkovGenText::MarkovGenText(size_t a_CountWordInPrefix):
    mCountWordInPrefix(a_CountWordInPrefix)
{

}
//------------------------------------------------------------------------------
bool inline IsEndSimbol(char simbol)
{
    return (simbol== ' ') ||(simbol == ',')||(simbol == '.');
}
//------------------------------------------------------------------------------
void MarkovGenText::Add(const std::string &a_Text)
{
    std::string world;
    Prefix prefix;
    for (char simbol: a_Text)
    {
        world.push_back(simbol);
        if (IsEndSimbol(simbol))
        {
            if (prefix.size() >= mCountWordInPrefix)
            {
                mStateTable[prefix].push_back( world);
                prefix.pop_front();

            }
            //prefix.emplace_back(world);
            prefix.push_back(std::move(world));
        }


    }

}
//------------------------------------------------------------------------------
std::string MarkovGenText::GenText(size_t a_CountWord)
{
    std::string str;
    Prefix prefix = mStateTable.begin()->first;
    for (size_t i = 0; i < a_CountWord; ++i)
    {
        std::vector<std::string>& suf = mStateTable[prefix];
        if (0 == suf.size())
            return str;
        std::string world = suf[rand() % suf.size()];
        prefix.pop_front();
        str.append(world);
        prefix.push_back(std::move(world));
    }

    return str;
}
//------------------------------------------------------------------------------
