#ifndef MARKOVGENTEXT_H
#define MARKOVGENTEXT_H
#include <string>
#include <map>
#include <unordered_map>
#include <deque>
#include <vector>
//------------------------------------------------------------------------------
typedef std::deque<std::string> Prefix;
//------------------------------------------------------------------------------
class MarkovGenText
{
public:
    MarkovGenText(size_t a_CountWordInPrefix);
    void Add(const std::string &a_Text);
    void Clear();
    std::string GenText(size_t a_CountWord);
    const std::map<Prefix, std::vector<std::string> > & ShowStateTable() const
    //const std::unordered_map<Prefix, std::vector<std::string> >& ShowStateTable() const
    {
        return mStateTable;
    }
private:
    //std::unordered_map<Prefix, std::vector<std::string> > mStateTable;
    std::map<Prefix, std::vector<std::string> > mStateTable;
    const size_t mCountWordInPrefix;
};
//------------------------------------------------------------------------------
#endif // MARKOVGENTEXT_H
